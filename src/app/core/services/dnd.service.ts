import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DndService {
  private draggable: any;

  constructor() { }

  setDraggable(draggable: any): void {
    this.draggable = draggable;
  }

  getDraggable(): any {
    return this.draggable;
  }

  clearDraggable(): any {
    this.draggable = null;
  }
}
