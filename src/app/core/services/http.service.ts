import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DashboardItemModel} from "../../features/dashboard/models/dashboard.model";
import {TaskModel} from "../../features/dashboard/models/tasks.model";

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) {

  }

  getDashboardData(): Observable<DashboardItemModel[]> {
    return this.httpClient.get<DashboardItemModel[]>('http://localhost:3000/boards');
  }

  setBoard(board: DashboardItemModel): Observable<DashboardItemModel> {
    return this.httpClient.post<DashboardItemModel>('http://localhost:3000/boards', board);
  }

  deleteBoard(boardId: string): Observable<DashboardItemModel> {
    return this.httpClient.delete<DashboardItemModel>(`http://localhost:3000/boards/${boardId}`);
  }

  updateBoard(board: DashboardItemModel): Observable<DashboardItemModel> {
    return this.httpClient.patch<DashboardItemModel>(`http://localhost:3000/boards/${board.id}`, board);
  }

  getTasks(): Observable<TaskModel[]> {
    return this.httpClient.get<TaskModel[]>('http://localhost:3000/tasks');
  }

  updateTask(task: TaskModel): Observable<TaskModel> {
    return this.httpClient.patch<TaskModel>(`http://localhost:3000/tasks/${task.id}`,
      task
    );
  }

  deleteTask(task: TaskModel): Observable<TaskModel> {
    return this.httpClient.delete<TaskModel>(`http://localhost:3000/tasks/${task.id}`);
  }

  setTask(task: TaskModel): Observable<TaskModel> {
    return this.httpClient.post<TaskModel>(`http://localhost:3000/tasks/`,
      task
    );
  }
}
