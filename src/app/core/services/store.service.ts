import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {DashboardItemModel} from "../../features/dashboard/models/dashboard.model";
import {HttpService} from "./http.service";
import {TaskModel} from "../../features/dashboard/models/tasks.model";

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  dashboardSubject$$: BehaviorSubject<DashboardItemModel[]> = new BehaviorSubject<DashboardItemModel[]>([]);
  tasksSubject$$: BehaviorSubject<TaskModel[]> = new BehaviorSubject<TaskModel[]>([]);

  constructor(private http: HttpService) {
  }


  fetchDashboard(): void {
    this.http.getDashboardData().subscribe((dashboard) => {
      this.dashboardSubject$$.next(dashboard);
    })
  }

  setBoard(board: DashboardItemModel): void {
    console.log('test')
    this.http.setBoard(board).subscribe(() => {
      this.fetchDashboard();
    });
  }

  deleteBoard(boardId: string): void {
    this.http.deleteBoard(boardId).subscribe(() => {
      this.fetchDashboard();
    });
  }

  updateBoard(board: DashboardItemModel): void {
    this.http.updateBoard(board).subscribe(() => {
      this.fetchDashboard();
    });
  }

  getTasksByBoardId(id: number): TaskModel[] {
    return this.tasksSubject$$.getValue().filter(({boardId}) => boardId === id);
  }

  fetchTasks(): void {
    this.http.getTasks().subscribe((tasks) => {
      this.tasksSubject$$.next(tasks);
    })
  }

  setTasks(tasks: TaskModel[]): void {
    this.tasksSubject$$.next(tasks);
  }

  setTask(task: TaskModel): void {
    this.http.setTask(task).subscribe((t) => {
      this.fetchTasks();
    });
  }

  updateTask(task: TaskModel): void {
    this.http.updateTask(task).subscribe((t) => {
      this.fetchTasks();
    });
  }

  deleteTask(task: TaskModel): void {
    this.http.deleteTask(task).subscribe(() => {
      this.fetchTasks();
    });
  }
}
