import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './features/dashboard/components/dashboard/dashboard.component';
import {BoardComponent} from "./features/board/components/board/board.component";
import {ShellComponent} from "./core/components/shell/shell.component";

const routes: Routes = [
  {path:'',component:ShellComponent, children:[
      {path:'',loadChildren:()=>import('./features/dashboard/dashboard.module').then((m)=>m.DashboardModule)},
      {path:'board',loadChildren:()=>import('./features/board/board.module').then((m)=>m.BoardModule)},
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
