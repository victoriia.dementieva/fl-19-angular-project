import {Directive, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {DndService} from "../../core/services/dnd.service";

@Directive({
  selector: '[appDndDraggable]'
})
export class DndDraggableDirective {
  @Input() list: any[] = [];
  @Input() item: any;

  @Output() onDrag: EventEmitter<any[]> = new EventEmitter<any[]>();

  private isDragging: boolean = false;

  constructor(private el: ElementRef, private dndService: DndService) {
  }

  @HostListener('mousedown', ['$event.target']) onMouseDown(event: any) {
    if(event !== this.el.nativeElement) {
      return;
    }

    this.isDragging = true;
    this.el.nativeElement.draggable = true;
    this.list = this.list.filter((el) => el.id !== this.item.id);
    this.onDrag.emit(this.list);
    this.dndService.setDraggable(this.item);
  }

  @HostListener('mouseup') onMouseUp() {
    this.isDragging = false;
    this.el.nativeElement.outerHTML = '';
  }
}
