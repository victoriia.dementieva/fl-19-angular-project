import {Directive, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {DndService} from "../../core/services/dnd.service";

@Directive({
  selector: '[appDndDropzone]'
})
export class DndDropzoneDirective {
  @Input() list: any[] = [];

  @Output() onDragEnd: EventEmitter<any> = new EventEmitter<any>();

  constructor(private el: ElementRef, private dndService: DndService) {
  }

  @HostListener('mouseup') onMouseUp() {
    if(!this.dndService.getDraggable()) {
      return;
    }

    this.list.push(this.dndService.getDraggable());
    this.onDragEnd.emit(this.dndService.getDraggable());
    this.dndService.clearDraggable();
  }
}
