import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrderByPipe} from './pipes/order-by.pipe';
import {DndDropzoneDirective} from './directives/dnd-dropzone.directive';
import {DndDraggableDirective} from './directives/dnd-draggable.directive';


@NgModule({
  declarations: [
    OrderByPipe,
    DndDropzoneDirective,
    DndDraggableDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    OrderByPipe,
    DndDropzoneDirective,
    DndDraggableDirective,
  ]
})
export class SharedModule {
}
