import { Pipe, PipeTransform } from '@angular/core';
import {orderBy} from "lodash";
import {TasksStatuses} from "../../features/dashboard/models/tasks.model";

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(array: any, sortBy: string, order: 'asc' | 'desc', type: string): any[] {
    const sortOrder = order ? order : 'asc'; // setting default ascending order

    if(type === 'date') {
      return orderBy(array.map((item: any) => {
        return {
          ...item,
          [sortBy]: new Date(item[sortBy]),
        }
      }), [sortBy], [sortOrder]);
    }

    if(type === 'todo') {
      return orderBy(array, [(item) => item.tasks.status === TasksStatuses.ToDo], [sortOrder]);
    }

    if(type === 'inProgress') {
      return orderBy(array, [(item) => item.tasks.status === TasksStatuses.InProgress], [sortOrder]);
    }

    if(type === 'done') {
      return orderBy(array, [(item) => item.tasks.status === TasksStatuses.Done], [sortOrder]);
    }


    // @ts-ignore
    return orderBy(array, [sortBy], [sortOrder]);
  }

}
