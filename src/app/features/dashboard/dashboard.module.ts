import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {DashboardItemComponent} from "./components/dashboard-item/dashboard-item.component";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {SharedModule} from "../../shared/shared.module";
import { AddBoardComponent } from './components/add-board/add-board.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [DashboardComponent, DashboardItemComponent, AddBoardComponent],
    imports: [
        CommonModule, DashboardRoutingModule,
        SharedModule, ReactiveFormsModule, FormsModule
    ]
})
export class DashboardModule { }
