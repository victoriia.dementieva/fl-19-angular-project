import {TaskModel} from "./tasks.model";

export interface DashboardItemModel {
  "id": number,
  "createdAt": string | null,
  "name": string | null,
  "description": string | null,
  "tasks"?: TaskModel[],
}
