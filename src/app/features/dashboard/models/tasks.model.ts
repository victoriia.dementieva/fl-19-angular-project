export interface TaskModel {
  "id": number,
  "createdAt": string,
  "name": string,
  "status": TasksStatuses,
  "boardId": number,
  "comments": string[],
}

export enum TasksStatuses {
  ToDo,
  InProgress,
  Done,
  Archived
}
