import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardItemComponent } from './dashboard-item.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {DashboardItemModel} from "../../models/dashboard.model";

describe('DashboardItemComponent', () => {
  let component: DashboardItemComponent;
  let fixture: ComponentFixture<DashboardItemComponent>;

  const boardMock: DashboardItemModel = {
      "name": "test",
      "description": "test desc",
      "createdAt": "2022-11-13T12:29:15.137Z",
      "id": 1
    };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardItemComponent ],
      imports: [HttpClientTestingModule],
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardItemComponent);
    component = fixture.componentInstance;
    component.item = {
      ...boardMock
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
