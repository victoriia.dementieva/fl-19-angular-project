import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DashboardItemModel} from "../../models/dashboard.model";
import {Router} from "@angular/router";
import {TaskModel, TasksStatuses} from "../../models/tasks.model";

@Component({
  selector: 'app-dashboard-item',
  templateUrl: './dashboard-item.component.html',
  styleUrls: ['./dashboard-item.component.scss']
})
export class DashboardItemComponent implements OnInit {
  @Input() item: DashboardItemModel | undefined;

  @Output() editClick = new EventEmitter<DashboardItemModel>();

  protected splitTasks: Record<TasksStatuses, TaskModel[]> = {
    [TasksStatuses.ToDo]: [],
    [TasksStatuses.InProgress]: [],
    [TasksStatuses.Done]: [],
    [TasksStatuses.Archived]: [],
  };

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.item?.tasks?.map((task) => {
      this.splitTasks[task.status].push(task);
    })
  }

  navigate(): void {
    this.router.navigate([`board/${this.item!.id}`])
  }

}
