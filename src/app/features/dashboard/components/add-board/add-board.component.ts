import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validator, Validators} from "@angular/forms";
import {DashboardItemModel} from "../../models/dashboard.model";
import {StoreService} from "../../../../core/services/store.service";

@Component({
  selector: 'app-add-board',
  templateUrl: './add-board.component.html',
  styleUrls: ['./add-board.component.scss']
})
export class AddBoardComponent implements OnInit {
  @Input() activeBoard: DashboardItemModel | undefined;
  @Input() dashboards!:DashboardItemModel[];

  @Output() onClose = new EventEmitter<void>();

  protected form: FormGroup = this.fb.group({
    'name': ['', Validators.required],
    'description': ['', Validators.required],
  });

  constructor(private fb: FormBuilder, private storeService: StoreService) {
  }

  ngOnInit(): void {
    this.fillForm();
  }

  private fillForm(): void {
    if (!this.activeBoard) {
      return;
    }

    this.form.get('name')?.setValue(this.activeBoard.name);
    this.form.get('description')?.setValue(this.activeBoard.description);
  }

  protected submitForm(): void {
    const board: DashboardItemModel = {
      ...this.form.getRawValue(),
      createdAt: this.activeBoard ? this.activeBoard.createdAt : new Date().toISOString(),
      id: this.activeBoard ? this.activeBoard.id : this.dashboards.length+1,
    };
    this.activeBoard ? this.storeService.updateBoard(board) : this.storeService.setBoard(board);
    this.onClose.emit();
  }

}
