import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AddBoardComponent} from './add-board.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {StoreService} from "../../../../core/services/store.service";

describe('AddBoardComponent', () => {
  let component: AddBoardComponent;
  let fixture: ComponentFixture<AddBoardComponent>;
  let storeServiceMock: StoreService;

  const dasboardsMock = [{
    "name": "test",
    "description": "test desc",
    "createdAt": "2022-11-13T12:29:15.137Z",
    "id": 1
  }];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AddBoardComponent],
      imports: [CommonModule, FormsModule, ReactiveFormsModule, HttpClientTestingModule]
    })
      .compileComponents();

    storeServiceMock = TestBed.inject(StoreService);
    fixture = TestBed.createComponent(AddBoardComponent);
    component = fixture.componentInstance;
    component.dashboards = [
      ...dasboardsMock,
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call a setBoard function of submit', () => {
    const input = fixture.debugElement.nativeElement.querySelector('input[data-test-id="input"]');
    const submit = fixture.debugElement.nativeElement.querySelector('button[data-test-id="submit"]');
    const spy = jasmine.createSpy();
    storeServiceMock.setBoard = spy;
    input.value = 'test';
    fixture.detectChanges();

    submit.click();

    expect(storeServiceMock.setBoard).toHaveBeenCalled();
  });
});
