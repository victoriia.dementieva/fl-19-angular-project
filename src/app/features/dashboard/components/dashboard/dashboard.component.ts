import {Component, OnInit} from '@angular/core';
import {DashboardItemModel} from "../../models/dashboard.model";
import {StoreService} from "../../../../core/services/store.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  name = 'Dashboard';
  protected boardsList: DashboardItemModel[] = [];
  protected filteredBoardsList: DashboardItemModel[] = []
  protected selectedSortField = 'name';
  protected isAddEditOpened = false;
  protected nameFilterString = '';
  protected taskFilterString = '';
  protected sortDirection: 'asc' | 'desc' = 'asc';
  protected activeBoard: DashboardItemModel | undefined;

  constructor(private storeService: StoreService) {
  }

  ngOnInit(): void {
    this.fetchBoards();

    this.storeService.dashboardSubject$$.subscribe((boards) => {
      this.boardsList = boards.map((board) => {
        board.tasks = this.storeService.getTasksByBoardId(board.id);

        return board;
      });

      this.filter();
    })
  }

  get sortType(): string {
    if (this.selectedSortField === 'createdAt') {
      return 'date';
    }

    if (this.selectedSortField === 'todo') {
      return 'todo';
    }

    if (this.selectedSortField === 'inProgress') {
      return 'inProgress';
    }

    if (this.selectedSortField === 'done') {
      return 'done';
    }

    return '';
  }

  setSort(field: string): void {
    this.selectedSortField = field;
  }

  setSortDirection(direction: 'asc' | 'desc'): void {
    this.sortDirection = direction;
  }

  fetchBoards(): void {
    this.storeService.fetchDashboard();
  }

  setAddEditOpen(value: boolean): void {
    this.isAddEditOpened = value;
  }

  setActiveBoard(board: DashboardItemModel): void {
    this.activeBoard = board;
    this.setAddEditOpen(true);
  }

  filter(): void {
    this.filteredBoardsList = this.boardsList.filter((item) => {
      return item.name?.includes(this.nameFilterString) && (!!item.tasks && this.taskFilterString ? item.tasks?.some(({name}) => name.includes(this.taskFilterString)) : true);
    })
  }
}
