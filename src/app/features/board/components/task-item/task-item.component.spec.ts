import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TaskItemComponent} from './task-item.component';
import {TaskModel} from "../../../dashboard/models/tasks.model";

describe('TaskItemComponent', () => {
  let component: TaskItemComponent;
  let fixture: ComponentFixture<TaskItemComponent>;
  const TaskMock: TaskModel = {
    "name": "test",
    "createdAt": "2022-11-13T12:29:22.854Z",
    "boardId": 1,
    "status": 0,
    "id": 2,
    "comments": []
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskItemComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(TaskItemComponent);
    component = fixture.componentInstance;
    component.task = {
      ...TaskMock,
    };
    component.list = [TaskMock];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
