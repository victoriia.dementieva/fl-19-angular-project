import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TaskModel} from "../../../dashboard/models/tasks.model";

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent implements OnInit {
  @Input() task!: TaskModel;
  @Input() list!: TaskModel[];

  @Output() updateList = new EventEmitter<TaskModel[]>();
  @Output() onEdit = new EventEmitter<TaskModel>();
  @Output() onDelete = new EventEmitter<TaskModel>();
  @Output() onArchive = new EventEmitter<TaskModel>();

  isEdit: boolean = false;
  isComment: boolean = false;
  newComment: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  protected toggleEditTask(): void {
    this.isEdit = !this.isEdit;
  }

  protected toggleAddCommentTask(): void {
    this.isComment = !this.isComment;
  }

  protected addComment(): void {
    if(!this.newComment) {
      return;
    }

    this.task.comments.push(this.newComment);
    this.changeComments();
  }

  protected deleteComment(index: number): void {
    this.task.comments = this.task.comments.filter((comment, i) => i !== index);
    this.changeComments();
  }

  protected changeComments(): void {
    this.isComment = false;
    this.onEdit.emit(this.task);
  }

  protected editTask(): void {
    this.isEdit = false;
    this.onEdit.emit(this.task);
  }

  protected deleteTask(): void {
    this.onDelete.emit(this.task);
  }
}
