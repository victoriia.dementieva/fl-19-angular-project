import {Component, ElementRef, OnInit} from '@angular/core';
import {StoreService} from "../../../../core/services/store.service";
import {TaskModel, TasksStatuses} from "../../../dashboard/models/tasks.model";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  private tasks: TaskModel[] = [];
  private rawTasks: TaskModel[] = [];
  private filteredTasks: TaskModel[] = [];
  private boardId: string = '';
  protected splitTasks: Record<TasksStatuses, TaskModel[]> = {
    [TasksStatuses.ToDo]: [],
    [TasksStatuses.InProgress]: [],
    [TasksStatuses.Done]: [],
    [TasksStatuses.Archived]: [],
  };
  protected nameFilterString = '';
  protected newTaskName = '';
  protected selectedSortField = 'name';
  protected sortDirection: 'asc' | 'desc' = 'asc';


  constructor(
    private storeService: StoreService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.getCurrentId();
    this.storeService.fetchTasks();
    this.storeService.tasksSubject$$.subscribe((tasks) => {
      this.rawTasks = tasks;
      this.tasks = tasks.filter(({boardId}) => `${boardId}` === this.boardId);
      this.filter();
    })
  }

  get sortType(): string {
    if (this.selectedSortField === 'createdAt') {
      return 'date';
    }

    return '';
  }
  filter(): void {
    this.filteredTasks = this.tasks.filter((item) => {
      return item.name?.includes(this.nameFilterString);
    })
    this.splitByStatus();
  }

  setSort(field: string): void {
    this.selectedSortField = field;
  }

  setSortDirection(direction: 'asc' | 'desc'): void {
    this.sortDirection = direction;
  }

  protected createTask(): void {
    if(!this.newTaskName) {
      return;
    }

    const task: TaskModel = {
      name: this.newTaskName,
      createdAt: new Date().toISOString(),
      boardId: +this.boardId,
      status: TasksStatuses.ToDo,
      id: this.rawTasks.length+1,
      comments: [],
    }
    this.storeService.setTask(task);
  }

  private splitByStatus(): void {
    this.splitTasks = {
      [TasksStatuses.ToDo]: [],
      [TasksStatuses.InProgress]: [],
      [TasksStatuses.Done]: [],
      [TasksStatuses.Archived]: [],
    };
    this.filteredTasks.map((task) => {
      this.splitTasks[task.status].push(task);
    });
  }

  private getCurrentId(): void {
    this.route.params.subscribe((params) => {
      this.boardId = params['id'];
    });
  }

  protected updateList(update: TaskModel[], status: string): void {
    this.splitTasks[status as unknown as TasksStatuses] = update;
  }

  protected updateTask(task: TaskModel, status: string): void {
    task.status = +status as unknown as TasksStatuses;
    this.storeService.updateTask(task);
  }

  protected editTask(task: TaskModel): void {
    this.storeService.updateTask(task);
  }

  protected deleteTask(task: TaskModel): void {
    this.storeService.deleteTask(task);

  }

  protected archiveTask(task: TaskModel): void {
    task.status = TasksStatuses.Archived;
    this.storeService.updateTask(task);
  }

  protected deleteBoard(): void {
    this.storeService.deleteBoard(this.boardId);
    this.router.navigate(['/']);
  }

  protected setColor(color: string, column: HTMLDivElement): void {
    column.classList.remove('red', 'blue', 'green')
    column.classList.add(color);
  }
}
