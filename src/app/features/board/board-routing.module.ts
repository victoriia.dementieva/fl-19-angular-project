import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {BoardComponent} from "./components/board/board.component";
import {BoardWrapperComponent} from "./components/board-wrapper/board-wrapper.component";

const routes: Routes = [
  {path:'',component:BoardWrapperComponent,
  children:[
    {
      path:':id', component:BoardComponent
    }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardRoutingModule { }
