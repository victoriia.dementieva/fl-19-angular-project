import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BoardComponent} from "./components/board/board.component";
import {BoardRoutingModule} from "./board-routing.module";
import { BoardWrapperComponent } from './components/board-wrapper/board-wrapper.component';
import {SharedModule} from "../../shared/shared.module";
import {FormsModule} from "@angular/forms";
import { TaskItemComponent } from './components/task-item/task-item.component';



@NgModule({
  declarations: [BoardComponent, BoardWrapperComponent, TaskItemComponent],
    imports: [
        CommonModule,
        BoardRoutingModule,
        SharedModule,
        FormsModule
    ],
  exports:[
    BoardComponent,
    BoardWrapperComponent
  ]
})
export class BoardModule { }
